﻿using System;

namespace Termsweeper
{
	class Program
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("Hello world");
			Map map = new Map();
			Console.SetCursorPosition(0,0);

			Console.BackgroundColor = ConsoleColor.White;
			Console.ForegroundColor = ConsoleColor.Black;

			while (true)
			{
				switch (Console.ReadKey(false).Key.ToString())
				{

					case "Enter":
						Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
						Console.Write("");
						Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
						break;

					case "UpArrow":
						Console.SetCursorPosition(Console.CursorLeft, Console.CursorTop - 1);
						break;

					case "DownArrow":
						Console.SetCursorPosition(Console.CursorLeft, Console.CursorTop + 1);
						break;

					case "LeftArrow":
						Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
						break;

					case "RightArrow":
						Console.SetCursorPosition(Console.CursorLeft + 1, Console.CursorTop);
						break;
					default:
						//revert and erase printed character
						Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
						Console.Write("█");
						Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
						break;
				}
			}
		}
	}	

	class Map
	{
		private int Width;
		private int Height;
		public char[,] Tiles; //contains a number from 0 to 8, or the unicode character '' for a bomb
		public Map(int sizeX = 40, int sizeY = 20, int chanceOfMine = 15)
		{
			this.Tiles = new char[sizeX, sizeY];
			this.Width = sizeX;
			this.Height = sizeY;

			Console.Clear();
			Console.ForegroundColor = ConsoleColor.White;

			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					Console.SetCursorPosition(x, y);
					Console.WriteLine("█");
					this.Tiles[x,y] = '0';

					Random rnd = new Random();
					//serves to allow the setting of chances
					//of mines appearing in each tile
					bool[] tmp = new bool[100];

					for (int k = 0; k < 100; k++)
					{
						if (k < chanceOfMine)
						{
							tmp[k] = true; //fills up as many of 100 values as there are chances of a mine
								    //being spawned in percentages. For example, if the chances of a mine
								    //spawning is 25% for each tile, then 25 of the 100 values will be set to
								    //true, the other 75 will remain set to false
						}
						else
						{
							tmp[k] = false;
						}
					}

					//now we choose a random number from 0 to 99. If the value under the
					//index of this random number is 'true' within the 'tmp' array, we
					//put a mine on the current tile
				
					if (tmp[rnd.Next(100)] == true)
					{
						this.Tiles[x,y] = '';
					}
					else
					{
						//we check the number of mines around the current tile 
						//and set that number as the value

						//left+up = x-1, y-1
						//up = x, y-1
						//right+up = x+1, y-1
						//left = x-1, y
						//right = x+1, y
						//left+down = x-1, y+1
						//down = x, y+1
						//right+down = x+1, y+1

						int num = 0;

						try
						{
							if (this.Tiles[x-1, y-1] == '')
							{
								num++;
							}
						} catch (IndexOutOfRangeException)
						{
							//ignore
						}

						try
						{
							if (this.Tiles[x, y-1] == '')
							{
								num++;
							}
						} catch (IndexOutOfRangeException)
						{
							//ignore
						}

						try
						{
							if (this.Tiles[x+1, y-1] == '')
							{
								num++;
							}
						} catch (IndexOutOfRangeException)
						{
							//ignore
						}

						try
						{
							if (this.Tiles[x-1, y] == '')
							{
								num++;
							}
						} catch (IndexOutOfRangeException)
						{
							//ignore
						}

						try
						{
							if (this.Tiles[x+1, y] == '')
							{
								num++;
							}
						} catch (IndexOutOfRangeException)
						{
							//ignore
						}

						try
						{
							if (this.Tiles[x-1, y+1] == '')
							{
								num++;
							}
						} catch (IndexOutOfRangeException)
						{
							//ignore
						}

						try
						{
							if (this.Tiles[x, y+1] == '')
							{
								num++;
							}
						} catch (IndexOutOfRangeException)
						{
							//ignore
						}

						try
						{
							if (this.Tiles[x+1, y+1] == '')
							{
								num++;
							}
						} catch (IndexOutOfRangeException)
						{
							//ignore
						}

						this.Tiles[x,y] = (char)num;
					}
				}
			}
		}
	}
}
